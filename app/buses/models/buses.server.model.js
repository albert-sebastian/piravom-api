const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let busSchema = new Schema({
	title: String,
	startTime: Number,
	toPlaces: [String],
	tags: [String]
});

mongoose.model('bus', busSchema);